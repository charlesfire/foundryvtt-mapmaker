import fs from "fs-extra";
const { readFile, writeFile } = fs;

const original =
`export function generatePlugin(externalPaths, exclude = [])
{
   return {
      name: 'typhonjs-fvtt-runtime-lib',
      options(opts)
      {
         // Used in local plugin to filter external opts.
         const externalOpts = Object.keys(externalPaths).filter((entry) => !exclude.includes(entry))

         opts.external = Array.isArray(opts.external) ? [...externalOpts, ...opts.external] : externalOpts;

         if (Array.isArray(opts.output))
         {
            for (const outputOpts of opts.output)
            {
               outputOpts.paths = typeof outputOpts.paths === 'object' ? { ...outputOpts.paths, ...externalPaths } :
                externalPaths;
            }
         }
         else if (typeof opts.output === 'object')
         {
            opts.output.paths = typeof opts.output.paths === 'object' ? { ...opts.output.paths, ...externalPaths } :
             externalPaths;
         }
      }
   };
}`;

const fixed =
`export function generatePlugin(externalPaths, exclude = [])
{
   return {
      name: 'typhonjs-fvtt-runtime-lib',
      options(opts)
      {
         // Used in local plugin to filter external opts.
         const externalOpts = Object.keys(externalPaths).filter((entry) => !exclude.includes(entry))

         opts.external = Array.isArray(opts.external) ? [...externalOpts, ...opts.external] : externalOpts;
      },
      outputOptions(opts)
      {
         opts.paths = typeof opts.paths === 'object' ? { ...opts.paths, ...externalPaths } : externalPaths;
      }
   };
}`;

readFile('./node_modules/@typhonjs-fvtt/runtime/.rollup/generatePlugin.js', (err, data) => {
  if (data.compare(Buffer.from(fixed)) == 0) {
    writeFile('./node_modules/@typhonjs-fvtt/runtime/.rollup/generatePlugin.js', original);
  } else {
    writeFile('./node_modules/@typhonjs-fvtt/runtime/.rollup/generatePlugin.js', fixed);
  }
});