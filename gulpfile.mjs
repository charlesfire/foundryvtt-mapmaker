import gulp from 'gulp';
const { series, src, dest, parallel } = gulp;

import { rollup } from 'rollup';
import typescript from '@rollup/plugin-typescript';
import del from 'del';
import fs from 'fs-extra';
import path from 'path';
import svelte from 'rollup-plugin-svelte';
import sveltePreprocess from 'svelte-preprocess';
import css from 'rollup-plugin-css-only';
import resolve from '@rollup/plugin-node-resolve';
import { typhonjsRuntime } from '@typhonjs-fvtt/runtime/rollup';

const DYNAMIC_TYPHONJS = true;
const SOURCEMAPS = true;

function bundle() {
  return rollup({
    input: './src/scripts/init.ts',
    plugins: [
      svelte({ preprocess: sveltePreprocess({ sourceMap: SOURCEMAPS }) }),
      typescript({ sourceMap: SOURCEMAPS, inlineSources: true }),
      css({ output: 'bundle.css' }),
      resolve({
        browser: true,
        dedupe: ['svelte'],
      }),
      DYNAMIC_TYPHONJS && typhonjsRuntime(),
    ],
  })
  .then(bundle => {
    return bundle.write({
      file: './dist/bundle.esm.js',
      format: 'es',
      sourcemap: SOURCEMAPS,
    });
  });
}

function deleteDist() {
  return del('./dist/*');
}

function copyLanguages() {
  return src('./src/lang/*.json').pipe(dest('./dist/lang/'))
}

function copyPacks() {
  return src('./src/packs/*.json').pipe(dest('./dist/packs/'))
}

function copyStyles() {
  return src('./src/styles/*.css').pipe(dest('./dist/styles/'))
}

function copyTemplates() {
  return src('./src/templates/*.hbs').pipe(dest('./dist/templates'))
}

function copyManifest() {
  return src('./src/module.json').pipe(dest('./dist'))
}

const copyResources = series(copyLanguages, copyPacks, copyStyles, copyTemplates, copyManifest);

async function getLinkDir() {
	const name = fs.readJSONSync('./src/module.json').name;
	const config = fs.readJSONSync('./foundryconfig.json');

  try {
		let dataDir;
		if (config.dataPath) {
			if (!fs.existsSync(path.join(config.dataPath, 'Data')))
				throw Error('User Data path invalid, no Data directory found');
      dataDir = path.join(config.dataPath, 'Data', 'modules', name);
		} else {
			throw Error('No User Data path defined in foundryconfig.json');
		}

    let foundryDir;
		if (config.foundryPath) {
			if (!fs.existsSync(config.foundryPath))
				throw Error('Foundry App path invalid, no Foundry directory found');
      foundryDir = config.foundryPath;
		} else {
			throw Error('No Foundry App path defined in foundryconfig.json');
		}

		return Promise.resolve({ dataDir, foundryDir });
	} catch (err) {
		Promise.reject(err);
	}
}

async function linkUserData() {
  getLinkDir().then(async ({ dataDir, foundryDir }) => {
    if (!fs.existsSync(dataDir)) {
			await fs.symlink(path.resolve('./dist'), dataDir, 'dir');
		}

    if (!fs.existsSync('./FoundryVTT')) {
			await fs.symlink(foundryDir, path.resolve('./FoundryVTT'), 'dir');
		}
  });
}

async function unlinkUserData() {
  getLinkDir().then(async ({ dataDir }) => {
    await fs.remove(dataDir);
    await fs.remove('./FoundryVTT');
  });
}

export const link = linkUserData;
export const unlink = unlinkUserData;
export const clean = series(deleteDist, unlink);
export const build = series(parallel(bundle, copyResources), link);
export const cleanBuild = series(clean, build);
export default build;