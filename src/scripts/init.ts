import BasicApplication from './view/BasicApplication';

Hooks.on('ready', () =>
{
  new BasicApplication().render(true, { focus: true });
  console.log("This code runs once core initialization is ready and game data is available.");
});