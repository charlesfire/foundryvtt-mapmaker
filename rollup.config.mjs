import typescript from '@rollup/plugin-typescript';
import svelte from 'rollup-plugin-svelte';
import sveltePreprocess from 'svelte-preprocess';
import resolve from '@rollup/plugin-node-resolve';
import { typhonjsRuntime } from '@typhonjs-fvtt/runtime/rollup';
import css from 'rollup-plugin-css-only';

import fs from 'fs-extra';
const { copyFile } = fs;

const DYNAMIC_TYPHONJS = true;
const SOURCEMAPS = true;

export default () =>
{
  return [
    {
      input: './src/scripts/init.ts',
      output: {
        file: './dist/bundle.esm.js',
        format: 'es',
        sourcemap: SOURCEMAPS,
      },
      plugins: [
        svelte({ preprocess: sveltePreprocess({ sourceMap: SOURCEMAPS }) }),
        typescript({ sourceMap: SOURCEMAPS, inlineSources: true }),
        css({ output: 'bundle.css' }),
        resolve({
          browser: true,
          dedupe: ['svelte'],
        }),
        DYNAMIC_TYPHONJS && typhonjsRuntime(),
      ]
    }
  ];
};

copyFile('./src/module.json', './dist/module.json');
